#!/usr/bin/env python2
from __future__ import print_function
import requests
import time
import os.path
from markup import markup

title = "Seashell User Count"
header = "How many people are using Seashell right now?"
style = ('style.css',)
output_file = 'seashell_counter.html'


while not os.path.isfile('__stop'):
    page = markup.page()
    page.meta(http_equiv="refresh", content="29")
    footer = time.strftime("Last checked: %a, %d %b %Y %H:%M:%S (local server time)")
    page.init(title=title, css=style)
    page.h1(header)

    total = 0
    print("OK")

    fileName = 'count'
    if os.path.isfile(fileName):
        time_difference = time.time() - os.path.getmtime(fileName) 
        print (time_difference)
        if time_difference < 120:
            f = open(fileName, 'r')
            try: 
                total += int(f.readline())
                print(total)
            except ValueError:
                print("WTF")
                pass
            f.close()
            print(fileName)

    total = max(total - 3, total) # fix for grep process showing up
    page.p(total)
    page.p(footer, class_='footer')

    f = open(output_file, 'w')
    print(page, file=f)
    f.close()

    time.sleep(10)
