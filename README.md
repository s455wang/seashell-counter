# Seashell User Count

gather_script connects to the three servers and executes a command pipeline to get unique Seashell users.

main_counter.py looks for the output of gather_script in the file "count" and generates the static HTML page with results.
